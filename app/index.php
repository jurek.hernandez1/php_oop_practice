<?php

namespace Jurek\Practicando;

require_once "./vendor/autoload.php";

use Jurek\Practicando\Class\Armor\BronzeArmor;
use Jurek\Practicando\Class\Armor\GoldArmor;
//use Jurek\Practicando\Class\Armor\LeatherArmor;
use Jurek\Practicando\Class\Unit\Soldier;
use Jurek\Practicando\Class\Weapon\FireScepter;
use Jurek\Practicando\Class\Weapon\LongSword;
//use Jurek\Practicando\Class\Weapon\Sword;
use Jurek\Practicando\Faccades\Log;
use Jurek\Practicando\Class\FileLogger;
use Jurek\Practicando\Class\HtmlLogger;

//Log::setLogger(new FileLogger());
Log::setLogger(new HtmlLogger());

$soldier1 = new Soldier("Arturo", 100, new GoldArmor, new LongSword);
$soldier2 = new Soldier("Frodo", 100, new BronzeArmor, new FireScepter);
$soldier1->attack($soldier2);
$soldier2->changeArmor(new GoldArmor);
$soldier1->attack($soldier2);
$soldier2->attack($soldier1);