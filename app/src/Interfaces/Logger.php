<?php

namespace Jurek\Practicando\Interfaces;

interface Logger
{
    public function info(string $message);
}