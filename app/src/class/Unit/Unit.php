<?php

namespace Jurek\Practicando\Class\Unit;

use Jurek\Practicando\Class\Armor\Armor;
use Jurek\Practicando\Class\Armor\MissingArmor;
use Jurek\Practicando\Class\Weapon\Weapon;
use Jurek\Practicando\Class\Weapon\WoodenStick;
use Jurek\Practicando\Class\Attack;
use Jurek\Practicando\Faccades\Log;

class Unit
{
    protected string $name;
    protected int $hp;
    protected Armor $armor;
    protected Weapon $weapon;

    function __construct(string $name, int $hp = 100, Armor $armor = null,  Weapon $weapon = null)
    {
        $this->name = $name;
        $this->hp = $hp;
        if (is_null($armor)) {
            $this->armor = new MissingArmor;
        } else {
            $this->armor = $armor;
        }

        if (is_null($weapon)) {
            $this->weapon = new WoodenStick;
        } else {
            $this->weapon = $weapon;
        }
    }

    public function getName()
    {
        return $this->name;
    }

    function attack(Unit $opponent)
    {
        $attack = $this->weapon->createAttack();
        $attack->getDescription($this, $opponent);
        $opponent->takeDamage($attack);
    }


    public function takeDamage(Attack $attack)
    {
        $this->hp = $this->hp - $this->armor->absorbDamage($attack);
        if ($this->hp <= 0) {
            Log::info("$this->name Murio en la batalla");
            exit;
        }
        Log::info("{$this->name} quedo con {$this->hp} Hp tras el ataque");
    }

    public function changeArmor(Armor $armor)
    {
        Log::info("{$this->name} Cambiar su {$this->armor->name} por {$armor->name} ");
        $this->armor = $armor;
    }
}
