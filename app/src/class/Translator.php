<?php

namespace Jurek\Practicando\Class;

class Translator
{
    protected static $messages = [
        'Bow_attack' => ':unit: Dispara una fecha a :opponent:',
        'FireScepter_attack' => '<p style="display:inline;"> :unit: lanza una </p> <h4 style="color:red; display:inline;">bola de fuego</h4> a :opponent:',
        'LongBow_attack' => ':unit: Dispara una fecha potente a :opponent:',
        'LongSword_attack' => ':unit: ataca con su espada a :opponent:',
        'Sword_attack' => ':unit: ataca con su espada larga a :opponent:',
        'WoodenStick_attack' => ':unit: le lanza un palo a :opponent: ',
    ];

    public static function getMessages(string $key, array $params): string
    {
        if (!static::keyExists($key)) {
            return "[$key]";
        }
        return static::replaceParams(static::$messages[$key], $params);
    }

    protected static function keyExists(string $key)
    {
        return isset(static::$messages[$key]);
    }

    protected static function replaceParams(string $message, array $params): string
    {
        foreach ($params as $key => $value) {
            $message = str_replace(':' . $key . ':', $value, $message);
        }
        return $message;
    }
}