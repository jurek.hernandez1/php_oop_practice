<?php

namespace Jurek\Practicando\class;

use Jurek\Practicando\Interfaces\Logger;

class HtmlLogger implements Logger
{
    public function info(string $message)
    {
        print_r("<p> $message </p>");
    }
}