<?php

namespace Jurek\Practicando\class;

use Jurek\Practicando\Interfaces\Logger;

class FileLogger implements Logger
{
    public function info(string $message)
    {
        $fecha = date('Y-m-d');

        file_put_contents(
            //'../storages/logS.txt',
            __DIR__ . "/../../storages/log_$fecha.txt",
            "(" . date("Y-m-d H:i:s") . ") " . $message . "\n",
            FILE_APPEND
        );
    }
}