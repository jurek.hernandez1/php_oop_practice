<?php

namespace Jurek\Practicando\Class\Weapon;

class WoodenStick extends Weapon
{
    public int $damage = 1;
    public string $name = "WoodenStick";
    protected bool $magicDamage = false;
}