<?php
namespace Jurek\Practicando\Class\Weapon;

Class Bow extends Weapon
{
    public int $damage=25;
    public string $name = "Bow";
}