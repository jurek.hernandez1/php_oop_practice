<?php

namespace Jurek\Practicando\Class\Weapon;

class LongSword extends Weapon
{
    protected int $damage = 40;
    protected string $name = "LongSword";
    protected bool $magicDamage = false;
}