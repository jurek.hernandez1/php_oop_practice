<?php
namespace Jurek\Practicando\Class\Weapon;

Class Sword extends Weapon
{
    public int $damage=20;
    public string $name = "Sword";
}