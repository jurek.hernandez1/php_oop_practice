<?php
namespace Jurek\Practicando\Class\Weapon;

Class LongBow extends Weapon
{
    public int $damage=50;
    public string $name = "LongBow";
}