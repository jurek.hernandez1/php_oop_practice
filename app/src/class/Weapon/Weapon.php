<?php

namespace Jurek\Practicando\Class\Weapon;

use Jurek\Practicando\Class\Attack;

abstract class Weapon
{
    protected int $damage = 10;
    protected string $name = "Weapon";
    protected bool $magicDamage = false;

    public function createAttack()
    {
        return new Attack($this->name, $this->damage, $this->magicDamage, $this->getClassName());
    }

    public function getClassName(): string
    {
        return str_replace('Jurek\Practicando\Class\Weapon\\', '', get_class($this) . '_attack');
    }
}
