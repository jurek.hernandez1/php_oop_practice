<?php

namespace Jurek\Practicando\Class\Weapon;

class FireScepter extends Weapon
{
    protected int $damage = 50;
    protected string $name = "FireScepter";
    protected bool $magicDamage = true;
}