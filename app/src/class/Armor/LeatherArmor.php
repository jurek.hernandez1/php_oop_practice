<?php
namespace Jurek\Practicando\Class\Armor;



class LeatherArmor extends Armor{
    public $name="LeatherArmor";
    function absorbDamage(int $damage){
        if(rand(0,1)){
            return $damage;
        }
        Utils::Log::info("Esquiva el ataque");
        return 0;
    }
}
