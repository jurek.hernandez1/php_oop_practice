<?php 
namespace Jurek\Practicando\Class\Armor;

use Jurek\Practicando\Class\Attack;

abstract class Armor{
    public $name="Armor";
    public $reduce=10;
    
    public function absorbDamage(Attack $attack)
    {
        if($attack->isPhysical()){
            return $attack->getDamage() / $this->reduce;
        }
        return $attack->getDamage();
    }
}
