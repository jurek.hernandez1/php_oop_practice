<?php

namespace Jurek\Practicando\class;

use Jurek\Practicando\Class\Unit\Unit;
use Jurek\Practicando\Class\Translator;
use Jurek\Practicando\Faccades\Log;

class Attack
{
    protected string $nameArmor;
    protected int $damage;
    protected bool $magic;
    protected string $keyMessageAttack;

    public function __construct(string $nameArmor, int $damage, bool $magic, string $keyMessageAttack)
    {
        $this->nameArmor = $nameArmor;
        $this->damage = $damage;
        $this->magic = $magic;
        $this->keyMessageAttack = $keyMessageAttack;
    }

    public function getDamage()
    {
        return $this->damage;
    }

    public function isMagical()
    {
        return $this->magic;
    }

    public function isPhysical()
    {
        return !$this->magic;
    }

    public function getDescription(Unit $attacker, Unit $opponent)
    {
        // return Log::info(str_replace([':unit', ':opponent'], [$attacker->getName(), $opponent->getName()], $this->description));

        $mensaje = Translator::getMessages($this->keyMessageAttack, [
            'unit' => $attacker->getName(),
            'opponent' => $opponent->getName(),
        ]);
        return Log::info($mensaje);
    }
}