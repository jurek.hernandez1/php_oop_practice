<?php

namespace Jurek\Practicando\Faccades;

use Jurek\Practicando\Interfaces\Logger;

class Log
{
    private static $logger;
    public static function setLogger(Logger $logger)
    {
        static::$logger = $logger;
    }

    public static function info($mensaje)
    {
        if (static::$logger) {
            static::$logger->info($mensaje);
        }
    }
}